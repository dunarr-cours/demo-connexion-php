<?php
require_once "db.php";
if (!empty($_POST["username"])){
    $request = $pdo->prepare("INSERT INTO users (username, password, is_admin) VALUE (:username, :password, :is_admin)");
    $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
    $request->execute([
        "username" => $_POST["username"],
        "password" => $hash,
        "is_admin" => $_POST["is_admin"]
    ]);
    echo "utilisateur créé";
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form method="post">
    <input type="text" name="username">
    <input type="password" name="password">
    <label> 
        <input type="radio" name="is_admin" value="1">
        admin
    </label>
    <label> 
        <input type="radio" name="is_admin" value="0">
        pas admin
    </label>
    <input type="submit">
</form>
</body>
</html>