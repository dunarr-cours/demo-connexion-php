<?php
session_start();
if(empty($_SESSION["is_loggedin"])){
    header("Location: /connexion.php");
    exit();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Accueuil</h1>
<?php if($_SESSION["is_admin"]){ ?>
    <a href="/user_list.php">Utilisateurs</a><br>
<?php } ?>
<a href="/interventions_list.php">Interventions</a><br>
</body>
</html>