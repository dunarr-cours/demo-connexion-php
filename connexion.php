<?php
session_start();
require_once "db.php";
if (!empty($_POST["username"])){
    $request = $pdo->prepare("SELECT * FROM users WHERE username = :username");
    $request->execute([
            "username" => $_POST["username"]
    ]);
    $user = $request->fetch(PDO::FETCH_ASSOC);
    if($user && password_verify($_POST["password"], $user["password"])){
        $_SESSION["is_loggedin"] = true;
        $_SESSION["is_admin"] = $user["is_admin"];
        header("Location: /");
        exit();
    } else {
        echo "identifiant ou mot de passe incorrects";
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Connexion</h1>
<form method="post">
    <input type="text" name="username">
    <input type="password" name="password">
    <input type="submit" value="Se connecter">
</form>
</body>
</html>